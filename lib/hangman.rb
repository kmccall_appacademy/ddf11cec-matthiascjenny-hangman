class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def guesser
    return @guesser
  end

  def referee
    return @referee
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    length.times { @board.push(nil) }
  end

  def update_board(pos, char)
    pos.each do |i|
      @board[i] = char
    end
    return @board
  end

  def take_turn
    gss = @guesser.guess
    pos = @referee.check_guess(gss)
    self.update_board(pos, gss)
    if @referee.secret_word == @board.join("")
      puts "Finished! The word was \"#{@board.join("")}.\""
    else
      @guesser.handle_response(gss, @board)
    end
  end

end

class HumanPlayer
  attr_reader :dictionary, :secret_word
  attr_accessor :secret_length

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    puts "Pick a secret word and tell me how long it is."
    length = gets.chomp.to_i
    return length
  end

  def check_guess(char)
    puts "My guess is \"#{char}.\" What positions does\
     \"#{char}\" appear in?"
     positions = gets.chomp.to_a
    return positions
  end

  def guess
    puts "Guess a character!"
    gss = gets.chomp
    return gss
  end

  def register_secret_length(length)
    puts "The secret word is #{length} characters long."
  end

  def handle_response(ch, array)
    puts "After adding \"#{ch},\" the board looks like this:\
      #{@board.join("")}"
  end

end

class ComputerPlayer
  attr_reader :dictionary, :secret_word
  attr_accessor :secret_length, :current_array

  def initialize(dictionary)
    @dictionary = dictionary
    @secret_word = ""
    @secret_length = nil
    @current_board = []
    @candidate_words = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    return @secret_word.length
  end

  def check_guess(char)
    check(@secret_word, char)
  end

  def register_secret_length(length)
    @secret_length = length
    @candidate_words.each do |word|
      if word.length != @secret_length
        @candidate_words.delete(word)
      end
    end
  end

  def guess(board)
    @candidate_words.each do |word|
      board.each_with_index do |el, i|
        if el == nil
          next
        elsif el != word[i]
          @candidate_words.delete(word)
        end
      end
    end
    hsh = Hash.new(0)
    @candidate_words.each do |word|
      word.each_char do |ch|
        hsh[ch] += 1
      end
    end
    board.each { |l| hsh[l] = 0 }
    letters = []
    hsh.keys.each do |k|
      if hsh[k] == hsh.values.sort[-1]
        letters.push(k)
      end
    end
    return letters.sample
  end

  def handle_response(ch, array)
    @candidate_words.each do |word|
      if reject?(ch, array, word)
        @candidate_words.delete(word)
      else
        next
      end
    end
  end

  def check(word,char)
    positions = []
    word.each_char.with_index do |ch, i|
      positions.push(i) if ch == char
    end
    return positions
  end

  def reject?(ch, array, word)
    if check(word, ch) == array
     return false
   else
     return true
   end
  end

  def candidate_words
    return @candidate_words
  end

end
